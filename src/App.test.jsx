import { describe, expect, it } from 'vitest';
import App from './App';
import { render, screen, userEvent } from './test/test-utils';
import { debug } from 'vitest-preview';

describe('Simple working test', () => {
  it('should increment count on click', async () => {
    render(<App />);
    userEvent.click(screen.getByTestId('button'));
    userEvent.click(screen.getByTestId('button'));
    userEvent.click(screen.getByTestId('button'));
    debug();

    expect(await screen.findByText(/count is/i)).toBeInTheDocument();
  });
});